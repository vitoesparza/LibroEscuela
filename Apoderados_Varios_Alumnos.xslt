<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

 		<xsl:template match="/">
 			<table cellpadding="2" cellspacing="2" border="1">
 				<tr>
 					<th>Nombre</th>
 					<th>Apellido</th>
 					<th>Rut</th>
 				</tr>
 				<xsl:apply-templates select="Apoderados"/>
 			</table>
 		</xsl:template>

 		<xsl:template match="Apoderados">
 			<xsl:apply-templates select="Apoderado"></xsl:apply-templates>
 		</xsl:template>

 		<xsl:template match="Apoderado">
 			<tr>
 				<td>
 					<xsl:value-of select="Nombre"></xsl:value-of>
 				</td>
 				<td>
 					<xsl:value-of select="Apellido"></xsl:value-of>
 				</td>
 				<td>
 					<xsl:value-of select="Rut"></xsl:value-of>
 				</td>
 			</tr>
 		</xsl:template>

 	</xsl:stylesheet>

