/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateLibroClaseData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
		try {
			libropackage.Estudiante llibropackageEstudiante = libropackage.EstudianteDAO.createEstudiante();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asistencia, anotaciones, nota, cursoid_fk, apoderado_id_fk
			libropackage.EstudianteDAO.save(llibropackageEstudiante);
			libropackage.Apoderado llibropackageApoderado = libropackage.ApoderadoDAO.createApoderado();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : estudiante
			libropackage.ApoderadoDAO.save(llibropackageApoderado);
			libropackage.Curso llibropackageCurso = libropackage.CursoDAO.createCurso();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : estudiante, asignatura, institucion_id_fk
			libropackage.CursoDAO.save(llibropackageCurso);
			libropackage.Asignatura llibropackageAsignatura = libropackage.AsignaturaDAO.createAsignatura();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : nota, planificacion, profesorid_fk, curso_id_fk
			libropackage.AsignaturaDAO.save(llibropackageAsignatura);
			libropackage.Planificacion llibropackagePlanificacion = libropackage.PlanificacionDAO.createPlanificacion();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asignatura_id_fk
			libropackage.PlanificacionDAO.save(llibropackagePlanificacion);
			libropackage.Nota llibropackageNota = libropackage.NotaDAO.createNota();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asignaturaid_pk, estudiante_id_fk
			libropackage.NotaDAO.save(llibropackageNota);
			libropackage.Institucion llibropackageInstitucion = libropackage.InstitucionDAO.createInstitucion();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : curso
			libropackage.InstitucionDAO.save(llibropackageInstitucion);
			libropackage.Asistencia llibropackageAsistencia = libropackage.AsistenciaDAO.createAsistencia();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : estudiante_id_fk
			libropackage.AsistenciaDAO.save(llibropackageAsistencia);
			libropackage.Anotaciones llibropackageAnotaciones = libropackage.AnotacionesDAO.createAnotaciones();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : profesor_id_fk, estudiante_id_fk
			libropackage.AnotacionesDAO.save(llibropackageAnotaciones);
			libropackage.Profesor llibropackageProfesor = libropackage.ProfesorDAO.createProfesor();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : asignatura, anotaciones
			libropackage.ProfesorDAO.save(llibropackageProfesor);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateLibroClaseData createLibroClaseData = new CreateLibroClaseData();
			try {
				createLibroClaseData.createTestData();
			}
			finally {
				libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
