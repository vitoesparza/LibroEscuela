/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteLibroClaseData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
		try {
			libropackage.Estudiante llibropackageEstudiante = libropackage.EstudianteDAO.loadEstudianteByQuery(null, null);
			// Delete the persistent object
			libropackage.EstudianteDAO.delete(llibropackageEstudiante);
			libropackage.Apoderado llibropackageApoderado = libropackage.ApoderadoDAO.loadApoderadoByQuery(null, null);
			// Delete the persistent object
			libropackage.ApoderadoDAO.delete(llibropackageApoderado);
			libropackage.Curso llibropackageCurso = libropackage.CursoDAO.loadCursoByQuery(null, null);
			// Delete the persistent object
			libropackage.CursoDAO.delete(llibropackageCurso);
			libropackage.Asignatura llibropackageAsignatura = libropackage.AsignaturaDAO.loadAsignaturaByQuery(null, null);
			// Delete the persistent object
			libropackage.AsignaturaDAO.delete(llibropackageAsignatura);
			libropackage.Planificacion llibropackagePlanificacion = libropackage.PlanificacionDAO.loadPlanificacionByQuery(null, null);
			// Delete the persistent object
			libropackage.PlanificacionDAO.delete(llibropackagePlanificacion);
			libropackage.Nota llibropackageNota = libropackage.NotaDAO.loadNotaByQuery(null, null);
			// Delete the persistent object
			libropackage.NotaDAO.delete(llibropackageNota);
			libropackage.Institucion llibropackageInstitucion = libropackage.InstitucionDAO.loadInstitucionByQuery(null, null);
			// Delete the persistent object
			libropackage.InstitucionDAO.delete(llibropackageInstitucion);
			libropackage.Asistencia llibropackageAsistencia = libropackage.AsistenciaDAO.loadAsistenciaByQuery(null, null);
			// Delete the persistent object
			libropackage.AsistenciaDAO.delete(llibropackageAsistencia);
			libropackage.Anotaciones llibropackageAnotaciones = libropackage.AnotacionesDAO.loadAnotacionesByQuery(null, null);
			// Delete the persistent object
			libropackage.AnotacionesDAO.delete(llibropackageAnotaciones);
			libropackage.Profesor llibropackageProfesor = libropackage.ProfesorDAO.loadProfesorByQuery(null, null);
			// Delete the persistent object
			libropackage.ProfesorDAO.delete(llibropackageProfesor);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteLibroClaseData deleteLibroClaseData = new DeleteLibroClaseData();
			try {
				deleteLibroClaseData.deleteTestData();
			}
			finally {
				libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
