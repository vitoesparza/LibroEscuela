/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateLibroClaseData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
		try {
			libropackage.Estudiante llibropackageEstudiante = libropackage.EstudianteDAO.loadEstudianteByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.EstudianteDAO.save(llibropackageEstudiante);
			libropackage.Apoderado llibropackageApoderado = libropackage.ApoderadoDAO.loadApoderadoByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.ApoderadoDAO.save(llibropackageApoderado);
			libropackage.Curso llibropackageCurso = libropackage.CursoDAO.loadCursoByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.CursoDAO.save(llibropackageCurso);
			libropackage.Asignatura llibropackageAsignatura = libropackage.AsignaturaDAO.loadAsignaturaByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.AsignaturaDAO.save(llibropackageAsignatura);
			libropackage.Planificacion llibropackagePlanificacion = libropackage.PlanificacionDAO.loadPlanificacionByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.PlanificacionDAO.save(llibropackagePlanificacion);
			libropackage.Nota llibropackageNota = libropackage.NotaDAO.loadNotaByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.NotaDAO.save(llibropackageNota);
			libropackage.Institucion llibropackageInstitucion = libropackage.InstitucionDAO.loadInstitucionByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.InstitucionDAO.save(llibropackageInstitucion);
			libropackage.Asistencia llibropackageAsistencia = libropackage.AsistenciaDAO.loadAsistenciaByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.AsistenciaDAO.save(llibropackageAsistencia);
			libropackage.Anotaciones llibropackageAnotaciones = libropackage.AnotacionesDAO.loadAnotacionesByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.AnotacionesDAO.save(llibropackageAnotaciones);
			libropackage.Profesor llibropackageProfesor = libropackage.ProfesorDAO.loadProfesorByQuery(null, null);
			// Update the properties of the persistent object
			libropackage.ProfesorDAO.save(llibropackageProfesor);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Estudiante by EstudianteCriteria");
		libropackage.EstudianteCriteria llibropackageEstudianteCriteria = new libropackage.EstudianteCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageEstudianteCriteria.id_pk.eq();
		System.out.println(llibropackageEstudianteCriteria.uniqueEstudiante());
		
		System.out.println("Retrieving Apoderado by ApoderadoCriteria");
		libropackage.ApoderadoCriteria llibropackageApoderadoCriteria = new libropackage.ApoderadoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageApoderadoCriteria.id_pk.eq();
		System.out.println(llibropackageApoderadoCriteria.uniqueApoderado());
		
		System.out.println("Retrieving Curso by CursoCriteria");
		libropackage.CursoCriteria llibropackageCursoCriteria = new libropackage.CursoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageCursoCriteria.id_pk.eq();
		System.out.println(llibropackageCursoCriteria.uniqueCurso());
		
		System.out.println("Retrieving Asignatura by AsignaturaCriteria");
		libropackage.AsignaturaCriteria llibropackageAsignaturaCriteria = new libropackage.AsignaturaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageAsignaturaCriteria.id_pk.eq();
		System.out.println(llibropackageAsignaturaCriteria.uniqueAsignatura());
		
		System.out.println("Retrieving Planificacion by PlanificacionCriteria");
		libropackage.PlanificacionCriteria llibropackagePlanificacionCriteria = new libropackage.PlanificacionCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackagePlanificacionCriteria.id_pk.eq();
		System.out.println(llibropackagePlanificacionCriteria.uniquePlanificacion());
		
		System.out.println("Retrieving Nota by NotaCriteria");
		libropackage.NotaCriteria llibropackageNotaCriteria = new libropackage.NotaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageNotaCriteria.id_pk.eq();
		System.out.println(llibropackageNotaCriteria.uniqueNota());
		
		System.out.println("Retrieving Institucion by InstitucionCriteria");
		libropackage.InstitucionCriteria llibropackageInstitucionCriteria = new libropackage.InstitucionCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageInstitucionCriteria.id_pk.eq();
		System.out.println(llibropackageInstitucionCriteria.uniqueInstitucion());
		
		System.out.println("Retrieving Asistencia by AsistenciaCriteria");
		libropackage.AsistenciaCriteria llibropackageAsistenciaCriteria = new libropackage.AsistenciaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageAsistenciaCriteria.id_pk.eq();
		System.out.println(llibropackageAsistenciaCriteria.uniqueAsistencia());
		
		System.out.println("Retrieving Anotaciones by AnotacionesCriteria");
		libropackage.AnotacionesCriteria llibropackageAnotacionesCriteria = new libropackage.AnotacionesCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageAnotacionesCriteria.id_pk.eq();
		System.out.println(llibropackageAnotacionesCriteria.uniqueAnotaciones());
		
		System.out.println("Retrieving Profesor by ProfesorCriteria");
		libropackage.ProfesorCriteria llibropackageProfesorCriteria = new libropackage.ProfesorCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//llibropackageProfesorCriteria.id_pk.eq();
		System.out.println(llibropackageProfesorCriteria.uniqueProfesor());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateLibroClaseData retrieveAndUpdateLibroClaseData = new RetrieveAndUpdateLibroClaseData();
			try {
				retrieveAndUpdateLibroClaseData.retrieveAndUpdateTestData();
				//retrieveAndUpdateLibroClaseData.retrieveByCriteria();
			}
			finally {
				libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
