/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListLibroClaseData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Estudiante...");
		libropackage.Estudiante[] libropackageEstudiantes = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
		int length = Math.min(libropackageEstudiantes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageEstudiantes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Apoderado...");
		libropackage.Apoderado[] libropackageApoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
		length = Math.min(libropackageApoderados.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageApoderados[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Curso...");
		libropackage.Curso[] libropackageCursos = libropackage.CursoDAO.listCursoByQuery(null, null);
		length = Math.min(libropackageCursos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageCursos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Asignatura...");
		libropackage.Asignatura[] libropackageAsignaturas = libropackage.AsignaturaDAO.listAsignaturaByQuery(null, null);
		length = Math.min(libropackageAsignaturas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageAsignaturas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Planificacion...");
		libropackage.Planificacion[] libropackagePlanificacions = libropackage.PlanificacionDAO.listPlanificacionByQuery(null, null);
		length = Math.min(libropackagePlanificacions.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackagePlanificacions[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Nota...");
		libropackage.Nota[] libropackageNotas = libropackage.NotaDAO.listNotaByQuery(null, null);
		length = Math.min(libropackageNotas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageNotas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Institucion...");
		libropackage.Institucion[] libropackageInstitucions = libropackage.InstitucionDAO.listInstitucionByQuery(null, null);
		length = Math.min(libropackageInstitucions.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageInstitucions[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Asistencia...");
		libropackage.Asistencia[] libropackageAsistencias = libropackage.AsistenciaDAO.listAsistenciaByQuery(null, null);
		length = Math.min(libropackageAsistencias.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageAsistencias[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Anotaciones...");
		libropackage.Anotaciones[] libropackageAnotacioneses = libropackage.AnotacionesDAO.listAnotacionesByQuery(null, null);
		length = Math.min(libropackageAnotacioneses.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageAnotacioneses[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Profesor...");
		libropackage.Profesor[] libropackageProfesors = libropackage.ProfesorDAO.listProfesorByQuery(null, null);
		length = Math.min(libropackageProfesors.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(libropackageProfesors[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing Estudiante by Criteria...");
		libropackage.EstudianteCriteria llibropackageEstudianteCriteria = new libropackage.EstudianteCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageEstudianteCriteria.id_pk.eq();
		llibropackageEstudianteCriteria.setMaxResults(ROW_COUNT);
		libropackage.Estudiante[] libropackageEstudiantes = llibropackageEstudianteCriteria.listEstudiante();
		int length =libropackageEstudiantes== null ? 0 : Math.min(libropackageEstudiantes.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageEstudiantes[i]);
		}
		System.out.println(length + " Estudiante record(s) retrieved."); 
		
		System.out.println("Listing Apoderado by Criteria...");
		libropackage.ApoderadoCriteria llibropackageApoderadoCriteria = new libropackage.ApoderadoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageApoderadoCriteria.id_pk.eq();
		llibropackageApoderadoCriteria.setMaxResults(ROW_COUNT);
		libropackage.Apoderado[] libropackageApoderados = llibropackageApoderadoCriteria.listApoderado();
		length =libropackageApoderados== null ? 0 : Math.min(libropackageApoderados.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageApoderados[i]);
		}
		System.out.println(length + " Apoderado record(s) retrieved."); 
		
		System.out.println("Listing Curso by Criteria...");
		libropackage.CursoCriteria llibropackageCursoCriteria = new libropackage.CursoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageCursoCriteria.id_pk.eq();
		llibropackageCursoCriteria.setMaxResults(ROW_COUNT);
		libropackage.Curso[] libropackageCursos = llibropackageCursoCriteria.listCurso();
		length =libropackageCursos== null ? 0 : Math.min(libropackageCursos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageCursos[i]);
		}
		System.out.println(length + " Curso record(s) retrieved."); 
		
		System.out.println("Listing Asignatura by Criteria...");
		libropackage.AsignaturaCriteria llibropackageAsignaturaCriteria = new libropackage.AsignaturaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageAsignaturaCriteria.id_pk.eq();
		llibropackageAsignaturaCriteria.setMaxResults(ROW_COUNT);
		libropackage.Asignatura[] libropackageAsignaturas = llibropackageAsignaturaCriteria.listAsignatura();
		length =libropackageAsignaturas== null ? 0 : Math.min(libropackageAsignaturas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageAsignaturas[i]);
		}
		System.out.println(length + " Asignatura record(s) retrieved."); 
		
		System.out.println("Listing Planificacion by Criteria...");
		libropackage.PlanificacionCriteria llibropackagePlanificacionCriteria = new libropackage.PlanificacionCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackagePlanificacionCriteria.id_pk.eq();
		llibropackagePlanificacionCriteria.setMaxResults(ROW_COUNT);
		libropackage.Planificacion[] libropackagePlanificacions = llibropackagePlanificacionCriteria.listPlanificacion();
		length =libropackagePlanificacions== null ? 0 : Math.min(libropackagePlanificacions.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackagePlanificacions[i]);
		}
		System.out.println(length + " Planificacion record(s) retrieved."); 
		
		System.out.println("Listing Nota by Criteria...");
		libropackage.NotaCriteria llibropackageNotaCriteria = new libropackage.NotaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageNotaCriteria.id_pk.eq();
		llibropackageNotaCriteria.setMaxResults(ROW_COUNT);
		libropackage.Nota[] libropackageNotas = llibropackageNotaCriteria.listNota();
		length =libropackageNotas== null ? 0 : Math.min(libropackageNotas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageNotas[i]);
		}
		System.out.println(length + " Nota record(s) retrieved."); 
		
		System.out.println("Listing Institucion by Criteria...");
		libropackage.InstitucionCriteria llibropackageInstitucionCriteria = new libropackage.InstitucionCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageInstitucionCriteria.id_pk.eq();
		llibropackageInstitucionCriteria.setMaxResults(ROW_COUNT);
		libropackage.Institucion[] libropackageInstitucions = llibropackageInstitucionCriteria.listInstitucion();
		length =libropackageInstitucions== null ? 0 : Math.min(libropackageInstitucions.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageInstitucions[i]);
		}
		System.out.println(length + " Institucion record(s) retrieved."); 
		
		System.out.println("Listing Asistencia by Criteria...");
		libropackage.AsistenciaCriteria llibropackageAsistenciaCriteria = new libropackage.AsistenciaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageAsistenciaCriteria.id_pk.eq();
		llibropackageAsistenciaCriteria.setMaxResults(ROW_COUNT);
		libropackage.Asistencia[] libropackageAsistencias = llibropackageAsistenciaCriteria.listAsistencia();
		length =libropackageAsistencias== null ? 0 : Math.min(libropackageAsistencias.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageAsistencias[i]);
		}
		System.out.println(length + " Asistencia record(s) retrieved."); 
		
		System.out.println("Listing Anotaciones by Criteria...");
		libropackage.AnotacionesCriteria llibropackageAnotacionesCriteria = new libropackage.AnotacionesCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageAnotacionesCriteria.id_pk.eq();
		llibropackageAnotacionesCriteria.setMaxResults(ROW_COUNT);
		libropackage.Anotaciones[] libropackageAnotacioneses = llibropackageAnotacionesCriteria.listAnotaciones();
		length =libropackageAnotacioneses== null ? 0 : Math.min(libropackageAnotacioneses.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageAnotacioneses[i]);
		}
		System.out.println(length + " Anotaciones record(s) retrieved."); 
		
		System.out.println("Listing Profesor by Criteria...");
		libropackage.ProfesorCriteria llibropackageProfesorCriteria = new libropackage.ProfesorCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//llibropackageProfesorCriteria.id_pk.eq();
		llibropackageProfesorCriteria.setMaxResults(ROW_COUNT);
		libropackage.Profesor[] libropackageProfesors = llibropackageProfesorCriteria.listProfesor();
		length =libropackageProfesors== null ? 0 : Math.min(libropackageProfesors.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(libropackageProfesors[i]);
		}
		System.out.println(length + " Profesor record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListLibroClaseData listLibroClaseData = new ListLibroClaseData();
			try {
				listLibroClaseData.listTestData();
				//listLibroClaseData.listByCriteria();
			}
			finally {
				libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
