package Datos;

import java.util.Random;

/**
 * en esta clase estan los nombres y apellidos para crear el xml.
 *
 * @author Ignacio
 */
public class NombresApellidos {
	private int ale1;
    private int ale2;
    //int ale3;
    Random r = new Random();
    //24
    private String [] apellidos={"Catal�n","Zambrotta","Salvatore","Silva","Danielez","Curvanez"
    ,"Morenazo","Schwansteigger","Papasthadopoulos","Fischio","Esparza","Palma","McDonals"
    ,"Lumbico","Angulo","Beron","Madrid","Balentines","Jacdaniels","Maxx","Heinz"
    ,"Burgos","Lee","Xun","Arroyo","Artigaz","Bauz�","Bas","Bilbao",
    "Espada","Elorza","Vasconavarro","Camino","Celdas","Paraguas","Lim�n","Villarrica",
    "Del Cerro","Madromo","Uzumaki","Hatake","Astor","Bunge","Olofsson","Berg",
    "lindgrend","Mattsson","Lundqvist","Coppola","Mirko","Mattia","Alessio","Farina","Luca",
    "D'amico","Colombo","Giuseppe","Rinaldi","Ricci","Rizzo","Amoretti","Volkova","Lebedev",
    "Golubev","Petrova","Kuznestsova"};
    
    //24
    private String [] nombres={"Marcos","Raul","Daniel","Roberto","Romero","Alberto"
    ,"Pablo","Juan","Lucas","Pedro","Jannette","Mariana","Denisse","Vanessa","Ignacio"
    ,"Carlos","Patricio","Cristiano","Lionel","Alexis","Alex","Felipe","Lucila","Manuela"
    ,"Ada","Adriana","Agustina","B�rbara","Berta","Camila","Candela","Casandra",
    "Dafne","Damila","Dina","Elsa","Elizabeth","Emilia","Eugenia",
    "Fabiola","Wendy","Wanda","Ximena","Zoe","Zacar�as","Ulises"};
    
    /**
     * 
     * @return retorna el nombre de algun alumno aleatorio 
     */
    public String nombres (){
        ale1=r.nextInt(nombres.length);
        return nombres[ale1];
    }
    
    /**
     * @return retorna el nombre del apoderado aleatorio 
     */
    public String apellidos(){
       ale2=r.nextInt(apellidos.length);
       return apellidos[ale2];
    }
}
