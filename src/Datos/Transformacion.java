package Datos;

import java.io.File;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * The Class Transformacion.
 *
 * @author Ignacio
 * @version 1.0.0
 */
public class Transformacion {
    /**
     * lo que hace es tomar un xsl y un xml para transformar en Excel,
     * HTML o word
     */
    public void transformar(String xsl, String xml, String nombre){
        System.out.println("Fuera Try Catch");
        try {

            TransformerFactory tff = TransformerFactory.newInstance();
            Transformer trans = tff.newTransformer(new StreamSource(new File(xsl)));
            StreamSource ss = new StreamSource(new File(xml));
            StreamResult sr = new StreamResult(new File(nombre));
            trans.transform(ss, sr);
            System.out.println("�xito");
        } catch (Exception e) {
            e.getMessage();
        }
    }
    }