package Datos;

import java.util.Random;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

/**
 * es donde se llenar� al base de datos
 * 
 * @author Ignacio
 *
 */

public class Crear {

	private static int RUTPROF = 5000;
	private static int RUTAPOD = 2000;
	private static int RUTEST = 0;
	private static final int ROW_COUNT = 1000;
	private NombresApellidos na = new NombresApellidos();
	Random ra = new Random();

	public void llenarDatabase() throws PersistentException {
		PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
		try {

			// libropackage.Nota libroPackageNota =
			// libropackage.NotaDAO.createNota();
			// // TODO Initialize the properties of the persistent object here,
			// the following properties must be initialized before saving :
			// asignaturaid_fk, estudiante_id_fk, planificacion_id_fk
			// libropackage.NotaDAO.save(libroPackageNota);
			//
			// libropackage.Asistencia libroPackageAsistencia =
			// libropackage.AsistenciaDAO.createAsistencia();
			// // TODO Initialize the properties of the persistent object here,
			// the following properties must be initialized before saving :
			// estudiante_id_fk
			// libropackage.AsistenciaDAO.save(libroPackageAsistencia);
			// libropackage.Anotaciones libroPackageAnotaciones =
			// libropackage.AnotacionesDAO.createAnotaciones();
			// // TODO Initialize the properties of the persistent object here,
			// the following properties must be initialized before saving :
			// profesor_id_fk, estudiante_id_fk
			// libropackage.AnotacionesDAO.save(libroPackageAnotaciones);

			cursos();
			apoderado();
			estudiantes();
			profesores();
			asignatura();
			asignatura();
			planificacion();
			notas();

			t.commit();
		} catch (Exception e) {
			t.rollback();
		}
	}

	/**
	 * donde se crean los profesores
	 * 
	 * @throws PersistentException
	 */
	public void profesores() throws PersistentException {
		for (int i = 0; i < 25; i++) {
			libropackage.Profesor profesor = libropackage.ProfesorDAO.createProfesor();
			profesor.setNombre(na.nombres());
			profesor.setApellido(na.apellidos());
			profesor.setRut(String.valueOf(RUTPROF++));
			libropackage.ProfesorDAO.save(profesor);
		}
	}

	/**
	 * aqui se crean los cursos
	 * 
	 * @throws PersistentException
	 */
	public void cursos() throws PersistentException {
		libropackage.Institucion institucion = libropackage.InstitucionDAO.createInstitucion();
		libropackage.InstitucionDAO.save(institucion);

		for (int i = 0; i < 8; i++) {
			libropackage.Curso curso = libropackage.CursoDAO.createCurso();
			curso.setInstitucion_id_fk(institucion);
			curso.setNivel(i + 1);
			curso.setLetra("A");
			libropackage.CursoDAO.save(curso);
		}

		for (int i = 0; i < 8; i++) {
			libropackage.Curso curso = libropackage.CursoDAO.createCurso();
			curso.setInstitucion_id_fk(institucion);
			curso.setNivel(i + 1);
			curso.setLetra("B");
			libropackage.CursoDAO.save(curso);
		}
	}

	/**
	 * se le asignan las asignaturas a cada profesor
	 * 
	 * @throws PersistentException
	 */
	public void asignatura() throws PersistentException {
		for (int i = 0; i < 16; i++) {

			libropackage.Curso curso = libropackage.CursoDAO.getCursoByORMID(i + 1);

			libropackage.Asignatura asignatura1 = libropackage.AsignaturaDAO.createAsignatura();
			libropackage.Profesor profesor1 = libropackage.ProfesorDAO.getProfesorByORMID(ra.nextInt(25) + 1);
			asignatura1.setCurso_id_fk(curso);
			asignatura1.setProfesorid_fk(profesor1);
			asignatura1.setMateria("Matem�ticas");
			libropackage.AsignaturaDAO.save(asignatura1);

			libropackage.Asignatura asignatura2 = libropackage.AsignaturaDAO.createAsignatura();
			asignatura2.setCurso_id_fk(curso);
			libropackage.Profesor profesor2 = libropackage.ProfesorDAO.getProfesorByORMID(ra.nextInt(25) + 1);
			asignatura2.setProfesorid_fk(profesor2);
			asignatura2.setMateria("Lenguaje");
			libropackage.AsignaturaDAO.save(asignatura2);

			libropackage.Asignatura asignatura3 = libropackage.AsignaturaDAO.createAsignatura();
			asignatura3.setCurso_id_fk(curso);
			libropackage.Profesor profesor3 = libropackage.ProfesorDAO.getProfesorByORMID(ra.nextInt(25) + 1);
			asignatura3.setProfesorid_fk(profesor3);
			asignatura3.setMateria("Historia");
			libropackage.AsignaturaDAO.save(asignatura3);

			libropackage.Asignatura asignatura4 = libropackage.AsignaturaDAO.createAsignatura();
			asignatura4.setCurso_id_fk(curso);
			libropackage.Profesor profesor4 = libropackage.ProfesorDAO.getProfesorByORMID(ra.nextInt(25) + 1);
			asignatura4.setProfesorid_fk(profesor4);
			asignatura4.setMateria("Ciencias");
			libropackage.AsignaturaDAO.save(asignatura4);

			libropackage.Asignatura asignatura5 = libropackage.AsignaturaDAO.createAsignatura();
			asignatura5.setCurso_id_fk(curso);
			libropackage.Profesor profesor5 = libropackage.ProfesorDAO.getProfesorByORMID(ra.nextInt(25) + 1);
			asignatura5.setProfesorid_fk(profesor5);
			asignatura5.setMateria("Ingl�s");
			libropackage.AsignaturaDAO.save(asignatura5);
		}
	}

	/**
	 * se guardan los apoderados en la base de datos
	 * 
	 * @throws PersistentException
	 */
	public void apoderado() throws PersistentException {
		for (int i = 0; i < 390; i++) {
			libropackage.Apoderado apoderado = libropackage.ApoderadoDAO.createApoderado();
			apoderado.setNombre(na.nombres());
			apoderado.setApellido(na.apellidos());
			apoderado.setRut(String.valueOf(RUTAPOD++));
			libropackage.ApoderadoDAO.save(apoderado);
		}
	}

	/**
	 * se guardan los estudiantes en la base y se le asigna un respectivo
	 * apoderado
	 * 
	 * @throws PersistentException
	 */
	public void estudiantes() throws PersistentException {
		int num;
		for (int j = 0; j < 16; j++) {
			libropackage.Curso curso = libropackage.CursoDAO.getCursoByORMID(j + 1);
			for (int i = 0; i < 30; i++) {
				num = ra.nextInt(390) + 1;
				libropackage.Apoderado apoderado = libropackage.ApoderadoDAO.getApoderadoByORMID(num);
				libropackage.Estudiante estudiante = libropackage.EstudianteDAO.createEstudiante();
				estudiante.setApoderado_id_fk(apoderado);
				estudiante.setCursoid_fk(curso);
				estudiante.setNombre(na.nombres());
				estudiante.setApellido(na.apellidos());
				estudiante.setRut(String.valueOf(RUTEST++));
				libropackage.EstudianteDAO.save(estudiante);
			}
		}
	}

	/**
	 * se guardan en la base las planificaciones por defecto
	 * 
	 * @throws PersistentException
	 */
	public void planificacion() throws PersistentException {
		for (int i = 0; i < 80; i++) {
			libropackage.Asignatura asignatura = libropackage.AsignaturaDAO.getAsignaturaByORMID(i + 1);
			for (int j = 0; j < 5; j++) {
				libropackage.Planificacion planificacion = libropackage.PlanificacionDAO.createPlanificacion();
				planificacion.setAsignatura_id_fk(asignatura);
				planificacion.setActividad("Actividad " + (j + 1));
				libropackage.PlanificacionDAO.save(planificacion);
			}
		}
	}

	/**
	 * donde se ingresan las notas por iniciales
	 * 
	 * @throws PersistentException
	 */
	public void notas() throws PersistentException {
		libropackage.Estudiante[] libropackageEstudiantes = libropackage.EstudianteDAO.listEstudianteByQuery(null,
				null);
		int length = Math.min(libropackageEstudiantes.length, ROW_COUNT);

		for (int i = 0; i < length; i++) {
			libropackage.Estudiante estudiante = libropackage.EstudianteDAO.getEstudianteByORMID(i + 1);
			for (int j = 0; j < 5; j++) {
				libropackage.Asignatura asignatura = libropackage.AsignaturaDAO.getAsignaturaByORMID(j + 1);
				for (int k = 0; k < 5; k++) {
					libropackage.Nota nota = libropackage.NotaDAO.createNota();
					nota.setEstudiante_id_fk(estudiante);
					nota.setAsignaturaid_pk(asignatura);
					nota.setNota(notasRandom()[k]);
					libropackage.NotaDAO.save(nota);
				}
			}
		}
	}

	/**
	 * aqui se generan notas random
	 * 
	 * @return
	 */
	public float[] notasRandom() {
		float[] notas = new float[5];
		if (Math.random() < 0.5) {
			for (int i = 0; i < notas.length; i++) {
				notas[i] = ra.nextInt(6) + 2;
			}
		} else {
			for (int i = 0; i < notas.length; i++) {
				notas[i] = ra.nextInt(6) + 2;
			}
		}
		return notas;
	}

	/**
	 * el es m�todo que se encarga de ejecutar todo lo de la clase.
	 */
	public static void metodoCrear() {
		try {
			Crear crear = new Crear();
			try {
				crear.llenarDatabase();
			} finally {
				libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
