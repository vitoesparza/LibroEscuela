package Datos.reportes.apoderado;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.orm.PersistentException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReportePlanificaciones {
	private Document doc;
	private int idAp;
	private String nombreAp;

	public ReportePlanificaciones() {
		try {
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = fact.newDocumentBuilder();
			doc = build.newDocument();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * este m�todo se encarga de ir creando el xml del reporte de las planificaciones del curso seleccionado.
	 * @param idEstudiante
	 * @param idApoderado
	 * @throws NumberFormatException
	 * @throws PersistentException
	 */

	public void generarDocument(ArrayList<String> idEstudiante, int idApoderado)
			throws NumberFormatException, PersistentException {
		idAp = idApoderado;
		libropackage.Apoderado ap = libropackage.ApoderadoDAO.getApoderadoByORMID(idAp);
		nombreAp = ap.getNombre();

		try {
			Element reporteAlumno = doc.createElement("ReporteAlumno");
			doc.appendChild(reporteAlumno);

			for (int j = 0; j < idEstudiante.size(); j++) {
				Element estudiante = doc.createElement("Estudiante");
				reporteAlumno.appendChild(estudiante);

				libropackage.Estudiante est = libropackage.EstudianteDAO
						.getEstudianteByORMID(Integer.parseInt(idEstudiante.get(j)));
				
				Element nombre = doc.createElement("Nombre");
				nombre.appendChild(doc.createTextNode(cleanString(est.getNombre())));
				estudiante.appendChild(nombre);

				Element apellido = doc.createElement("Apellido");
				apellido.appendChild(doc.createTextNode(cleanString(est.getApellido())));
				estudiante.appendChild(apellido);

				Element rut = doc.createElement("Rut");
				rut.appendChild(doc.createTextNode(est.getRut()));
				estudiante.appendChild(rut);
				
				
				Element asignaturas = doc.createElement("Asignaturas");
				estudiante.appendChild(asignaturas);
				
				for (int i = 0; i < est.getCursoid_fk().asignatura.toArray().length; i++) {
					
					Element asignatura = doc.createElement("Asignatura");
					asignatura.appendChild(doc.createTextNode(cleanString(String.valueOf(est.getCursoid_fk().asignatura.toArray()[i].getMateria()))));
					
					Element planificaciones = doc.createElement("Planificaciones");
					asignatura.appendChild(planificaciones);
					
					for (int k = 0; k < est.getCursoid_fk().asignatura.toArray()[i].planificacion.toArray().length; k++) {
	
						Element planificacion = doc.createElement("Planificacion");
						planificacion.appendChild(doc.createTextNode
								(cleanString(String.valueOf
										(est.getCursoid_fk().asignatura.toArray()[i].planificacion.toArray()[k].getActividad()))));
						planificaciones.appendChild(planificacion);
					}
					
					asignaturas.appendChild(asignatura);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * este m�todo crea el archivo xml escribiendolo en el destino seleccionado
	 * @throws TransformerException
	 */

	public void generarXML() throws TransformerException {
		try {
			TransformerFactory fact = TransformerFactory.newInstance();
			Transformer trans = fact.newTransformer();

			Source sourse = new DOMSource(doc);
			File file = new File("Planificaciones_"+nombreAp+".xml");
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			StreamResult res = new StreamResult(pw);

			trans.transform(sourse, res);
		} catch (TransformerConfigurationException | IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * este m�todo es para limbiar de car�cteres no permitidos a la hora de crear el xml
	 * @param texto resive el texto que se quiere limpiar
	 * @return retorna el texto limpio con los caracteres soportados
	 */
	public String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
	}
}
