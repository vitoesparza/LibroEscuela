package Datos.reportes.profesor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.orm.PersistentException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ReporteEstudiantes {
	private Document doc;
	private String nombreProfesor;
	private static final int ROW_COUNT = 30000;
	
	/**
	 * 
	 */

	public ReporteEstudiantes() {
		try {
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = fact.newDocumentBuilder();
			doc = build.newDocument();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * @param idEstudiante resive el id del estudiante.
	 * @param idProfesor resive el id del profesor.
	 * @param idAsignatura resive el id de la asignatura.
	 * @throws NumberFormatException error correspondiente a el formato num�rico.
	 * @throws PersistentException error de persistencia correspondiente a hibernate.
	 * 
	 * este m�todo se encarga de ir creando el xml del reporte de los estudiantes.
	 */
	public void generarDocument(ArrayList<String> idEstudiante, int idProfesor,int idAsignatura)
			throws NumberFormatException, PersistentException {
		System.out.println(idAsignatura);
		

		//System.out.println(idEstudiante.get(1));

		try {
			Element reporteAlumno = doc.createElement("ReporteAlumno");
			doc.appendChild(reporteAlumno);

			for (int j = 0; j < idEstudiante.size(); j++) {
				Element estudiante = doc.createElement("Estudiante");
				reporteAlumno.appendChild(estudiante);

				libropackage.Estudiante est = libropackage.EstudianteDAO
						.getEstudianteByORMID(Integer.parseInt(idEstudiante.get(j)));
				Element nombre = doc.createElement("Nombre");
				nombre.appendChild(doc.createTextNode(cleanString(est.getNombre())));
				estudiante.appendChild(nombre);

				Element apellido = doc.createElement("Apellido");
				apellido.appendChild(doc.createTextNode(cleanString(est.getApellido())));
				estudiante.appendChild(apellido);

				Element rut = doc.createElement("Rut");
				rut.appendChild(doc.createTextNode(est.getRut()));
				estudiante.appendChild(rut);

				Element promedios = doc.createElement("Promedios");
				estudiante.appendChild(promedios);

				libropackage.Nota[] notasOb = libropackage.NotaDAO.listNotaByQuery(null, null);
				int length = Math.min(notasOb.length, ROW_COUNT);
				int canNotas=1;
				float p=0;
				for (int k = 0; k < length; k++) {
					if (notasOb[k].getEstudiante_id_fk().getId_pk() == est.getId_pk()) {
						p = notasOb[k].getNota()+p;
						canNotas++;
					}
				}
				
				float promTotal=(p/canNotas);
				Element promedio = doc.createElement("Promedio");
				promedio.appendChild(doc.createTextNode(String.valueOf(promTotal)));
				promedios.appendChild(promedio);

				libropackage.Profesor profe = libropackage.ProfesorDAO.getProfesorByORMID(idProfesor);
				nombreProfesor = profe.getNombre()+" "+profe.getApellido()+" "+est.getCursoid_fk().getNivel()+" "
						+est.getCursoid_fk().getLetra();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * este m�todo crea el archivo xml escribiendolo en el destino seleccionado
	 * @throws TransformerException
	 */
	public void generarXML() throws TransformerException {
		try {
			TransformerFactory fact = TransformerFactory.newInstance();
			Transformer trans = fact.newTransformer();

			Source sourse = new DOMSource(doc);
			File file = new File("Promedios " + nombreProfesor + ".xml");
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			StreamResult res = new StreamResult(pw);

			trans.transform(sourse, res);
		} catch (TransformerConfigurationException | IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * este m�todo es para limbiar de car�cteres no permitidos a la hora de crear el xml
	 * @param texto resive el texto que se quiere limpiar
	 * @return retorna el texto limpio con los caracteres soportados
	 */
	public String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
	}
}
