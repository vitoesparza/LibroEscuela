package Datos.reportes.colegio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.orm.PersistentException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class AsistenciaPorEstudiante {
	private Document doc;
	private static final int ROW_COUNT = 1000;

	public AsistenciaPorEstudiante() {
		try {
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = fact.newDocumentBuilder();
			doc = build.newDocument();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * 
	 * @throws NumberFormatException
	 * @throws PersistentException
	 */
	public void generarDocument() throws NumberFormatException, PersistentException {
		libropackage.Asistencia[] asistenciasObj = libropackage.AsistenciaDAO.listAsistenciaByQuery(null, null);
		int length = Math.min(asistenciasObj.length, ROW_COUNT);
		int porcentaje;
		try {
			Element reporteAlumno = doc.createElement("ReporteAlumno");
			doc.appendChild(reporteAlumno);

			for (int j = 0; j < length; j++) {
				Element estudiante = doc.createElement("Estudiante");
				reporteAlumno.appendChild(estudiante);

				Element nombre = doc.createElement("Nombre");
				nombre.appendChild(
						doc.createTextNode(cleanString(asistenciasObj[j].getEstudiante_id_fk().getNombre())));
				estudiante.appendChild(nombre);

				Element apellido = doc.createElement("Apellido");
				apellido.appendChild(
						doc.createTextNode(cleanString(asistenciasObj[j].getEstudiante_id_fk().getApellido())));
				estudiante.appendChild(apellido);

				Element rut = doc.createElement("Rut");
				rut.appendChild(doc.createTextNode(asistenciasObj[j].getEstudiante_id_fk().getRut()));
				estudiante.appendChild(rut);

				Element asistencia = doc.createElement("Asistencia");
				porcentaje = (asistenciasObj[j].getRegistro() * 100) / 180;
				asistencia.appendChild(doc.createTextNode(String.valueOf(porcentaje + " % ")));
				estudiante.appendChild(asistencia);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * este m�todo crea el archivo xml escribiendolo en el destino seleccionado
	 * @throws TransformerException
	 */
	public void generarXML() throws TransformerException {
		try {
			TransformerFactory fact = TransformerFactory.newInstance();
			Transformer trans = fact.newTransformer();

			Source sourse = new DOMSource(doc);
			String a = String.valueOf(sourse.toString());
			System.out.println(a);
			File file = new File("AsistenciasEscuela.xml");
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			StreamResult res = new StreamResult(pw);

			trans.transform(sourse, res);
		} catch (TransformerConfigurationException | IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * este m�todo es para limbiar de car�cteres no permitidos a la hora de crear el xml
	 * @param texto resive el texto que se quiere limpiar
	 * @return retorna el texto limpio con los caracteres soportados
	 */
	public String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
	}
}
