package Datos.reportes.colegio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.orm.PersistentException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class EstudiantesReprobados {
	private Document doc;
	private String nombreProfesor;
	private static final int ROW_COUNT = 30000;

	public EstudiantesReprobados() {
		try {
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = fact.newDocumentBuilder();
			doc = build.newDocument();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	/**
	 * @throws NumberFormatException
	 * @throws PersistentException
	 */
	public void generarDocument() throws NumberFormatException, PersistentException {
		libropackage.Nota[] notasArray;
		libropackage.Estudiante[] estudiantes = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
		int tamEst = Math.min(estudiantes.length, ROW_COUNT);
		float pNotas = 0;
		float cantNotas = 0;

		try {
			Element reporteAlumno = doc.createElement("ReporteAlumno");
			doc.appendChild(reporteAlumno);

			for (int j = 0; j < tamEst; j++) {

				notasArray = estudiantes[j].nota.toArray();
				for (int i = 0; i < notasArray.length; i++) {
					pNotas = notasArray[i].getNota() + pNotas;
					cantNotas++;
				}
				float promTotal = (pNotas / cantNotas);
				
				if(promTotal<4.0){
					Element estudiante = doc.createElement("Estudiante");
					reporteAlumno.appendChild(estudiante);

					Element nombre = doc.createElement("Nombre");
					nombre.appendChild(doc.createTextNode(cleanString(estudiantes[j].getNombre())));
					estudiante.appendChild(nombre);

					Element apellido = doc.createElement("Apellido");
					apellido.appendChild(doc.createTextNode(cleanString(estudiantes[j].getApellido())));
					estudiante.appendChild(apellido);

					Element rut = doc.createElement("Rut");
					rut.appendChild(doc.createTextNode(estudiantes[j].getRut()));
					estudiante.appendChild(rut);

					Element promedio = doc.createElement("PromedioGeneral");
					promedio.appendChild(doc.createTextNode(String.valueOf(promTotal)));
					estudiante.appendChild(promedio);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * este m�todo crea el archivo xml escribiendolo en el destino seleccionado
	 * @throws TransformerException
	 */
	public void generarXML() throws TransformerException {
		try {
			TransformerFactory fact = TransformerFactory.newInstance();
			Transformer trans = fact.newTransformer();

			Source sourse = new DOMSource(doc);
			File file = new File("Estudiantes_Reprobados.xml");
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			StreamResult res = new StreamResult(pw);

			trans.transform(sourse, res);
		} catch (TransformerConfigurationException | IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * este m�todo es para limbiar de car�cteres no permitidos a la hora de crear el xml
	 * @param texto resive el texto que se quiere limpiar
	 * @return retorna el texto limpio con los caracteres soportados
	 */
	public String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
	}
}
