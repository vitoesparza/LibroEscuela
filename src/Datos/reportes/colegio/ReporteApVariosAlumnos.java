package Datos.reportes.colegio;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.orm.PersistentException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import libroescuela.Apoderado;

public class ReporteApVariosAlumnos {
	private Document document;
	private static final int ROW_COUNT = 1000;

	public ReporteApVariosAlumnos() {
		try {
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = fact.newDocumentBuilder();
			document = build.newDocument();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * 
	 * @param idAp
	 * @throws NumberFormatException
	 * @throws PersistentException
	 */
	public void generarDocument(Set<String> idAp) throws NumberFormatException, PersistentException {

		try {
			Element apoderados = document.createElement("Apoderados");
			document.appendChild(apoderados);

			for (String s : idAp) {

				Element apoderado = document.createElement("Apoderado");
				apoderados.appendChild(apoderado);

				libropackage.Apoderado a = libropackage.ApoderadoDAO.getApoderadoByORMID(Integer.parseInt(s));
				Element nombre = document.createElement("Nombre");
				nombre.appendChild(document.createTextNode(cleanString(a.getNombre())));
				apoderado.appendChild(nombre);

				Element apellido = document.createElement("Apellido");
				apellido.appendChild(document.createTextNode(cleanString(a.getApellido())));
				apoderado.appendChild(apellido);

				Element rut = document.createElement("Rut");
				rut.appendChild(document.createTextNode(a.getRut()));
				apoderado.appendChild(rut);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @throws TransformerException
	 */
	public void generarXML() throws TransformerException {
		try {
			TransformerFactory fact = TransformerFactory.newInstance();
			Transformer trans = fact.newTransformer();

			Source sourse = new DOMSource(document);
			File file = new File("Apoderados_Varios_Alumnos.xml");
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(fw);
			StreamResult res = new StreamResult(pw);

			trans.transform(sourse, res);
		} catch (TransformerConfigurationException | IOException e) {
			// TODO: handle exception
		}
	}
	
	/**
	 * este m�todo es para limbiar de car�cteres no permitidos a la hora de crear el xml
	 * @param texto resive el texto que se quiere limpiar
	 * @return retorna el texto limpio con los caracteres soportados
	 */
	public String cleanString(String texto) {
		texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
		texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
		return texto;
	}
}
