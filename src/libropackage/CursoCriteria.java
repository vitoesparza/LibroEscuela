/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CursoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression institucion_id_fkId;
	public final AssociationExpression institucion_id_fk;
	public final StringExpression letra;
	public final IntegerExpression nivel;
	public final CollectionExpression asignatura;
	public final CollectionExpression estudiante;
	
	public CursoCriteria(Criteria criteria) {
		super(criteria);
		id_pk = new IntegerExpression("id_pk", this);
		institucion_id_fkId = new IntegerExpression("institucion_id_fk.id_pk", this);
		institucion_id_fk = new AssociationExpression("institucion_id_fk", this);
		letra = new StringExpression("letra", this);
		nivel = new IntegerExpression("nivel", this);
		asignatura = new CollectionExpression("ORM_Asignatura", this);
		estudiante = new CollectionExpression("ORM_Estudiante", this);
	}
	
	public CursoCriteria(PersistentSession session) {
		this(session.createCriteria(Curso.class));
	}
	
	public CursoCriteria() throws PersistentException {
		this(libropackage.LibroClasePersistentManager.instance().getSession());
	}
	
	public InstitucionCriteria createInstitucion_id_fkCriteria() {
		return new InstitucionCriteria(createCriteria("institucion_id_fk"));
	}
	
	public AsignaturaCriteria createAsignaturaCriteria() {
		return new AsignaturaCriteria(createCriteria("ORM_Asignatura"));
	}
	
	public EstudianteCriteria createEstudianteCriteria() {
		return new EstudianteCriteria(createCriteria("ORM_Estudiante"));
	}
	
	public Curso uniqueCurso() {
		return (Curso) super.uniqueResult();
	}
	
	public Curso[] listCurso() {
		java.util.List list = super.list();
		return (Curso[]) list.toArray(new Curso[list.size()]);
	}
}

