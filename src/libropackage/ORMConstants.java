/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_ANOTACIONES_ESTUDIANTE_ID_FK = -634863687;
	
	final int KEY_ANOTACIONES_PROFESOR_ID_FK = 306884881;
	
	final int KEY_APODERADO_ESTUDIANTE = -852510826;
	
	final int KEY_ASIGNATURA_CURSO_ID_FK = 837022810;
	
	final int KEY_ASIGNATURA_NOTA = -1609688294;
	
	final int KEY_ASIGNATURA_PLANIFICACION = -747285158;
	
	final int KEY_ASIGNATURA_PROFESORID_FK = -794682093;
	
	final int KEY_ASISTENCIA_ESTUDIANTE_ID_FK = -363679775;
	
	final int KEY_CURSO_ASIGNATURA = 986371350;
	
	final int KEY_CURSO_ESTUDIANTE = 1452035445;
	
	final int KEY_CURSO_INSTITUCION_ID_FK = 2050242832;
	
	final int KEY_ESTUDIANTE_ANOTACIONES = -1755671051;
	
	final int KEY_ESTUDIANTE_APODERADO_ID_FK = -1719600648;
	
	final int KEY_ESTUDIANTE_ASISTENCIA = 1305407431;
	
	final int KEY_ESTUDIANTE_CURSOID_FK = -41890886;
	
	final int KEY_ESTUDIANTE_NOTA = 479462939;
	
	final int KEY_INSTITUCION_CURSO = -1245799386;
	
	final int KEY_NOTA_ASIGNATURAID_PK = -204725285;
	
	final int KEY_NOTA_ESTUDIANTE_ID_FK = -1993879091;
	
	final int KEY_PLANIFICACION_ASIGNATURA_ID_FK = -27759402;
	
	final int KEY_PROFESOR_ANOTACIONES = 738312717;
	
	final int KEY_PROFESOR_ASIGNATURA = 2059970248;
	
}
