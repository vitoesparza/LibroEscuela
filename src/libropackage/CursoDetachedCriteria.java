/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class CursoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression institucion_id_fkId;
	public final AssociationExpression institucion_id_fk;
	public final StringExpression letra;
	public final IntegerExpression nivel;
	public final CollectionExpression asignatura;
	public final CollectionExpression estudiante;
	
	public CursoDetachedCriteria() {
		super(libropackage.Curso.class, libropackage.CursoCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		institucion_id_fkId = new IntegerExpression("institucion_id_fk.id_pk", this.getDetachedCriteria());
		institucion_id_fk = new AssociationExpression("institucion_id_fk", this.getDetachedCriteria());
		letra = new StringExpression("letra", this.getDetachedCriteria());
		nivel = new IntegerExpression("nivel", this.getDetachedCriteria());
		asignatura = new CollectionExpression("ORM_Asignatura", this.getDetachedCriteria());
		estudiante = new CollectionExpression("ORM_Estudiante", this.getDetachedCriteria());
	}
	
	public CursoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, libropackage.CursoCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		institucion_id_fkId = new IntegerExpression("institucion_id_fk.id_pk", this.getDetachedCriteria());
		institucion_id_fk = new AssociationExpression("institucion_id_fk", this.getDetachedCriteria());
		letra = new StringExpression("letra", this.getDetachedCriteria());
		nivel = new IntegerExpression("nivel", this.getDetachedCriteria());
		asignatura = new CollectionExpression("ORM_Asignatura", this.getDetachedCriteria());
		estudiante = new CollectionExpression("ORM_Estudiante", this.getDetachedCriteria());
	}
	
	public InstitucionDetachedCriteria createInstitucion_id_fkCriteria() {
		return new InstitucionDetachedCriteria(createCriteria("institucion_id_fk"));
	}
	
	public AsignaturaDetachedCriteria createAsignaturaCriteria() {
		return new AsignaturaDetachedCriteria(createCriteria("ORM_Asignatura"));
	}
	
	public EstudianteDetachedCriteria createEstudianteCriteria() {
		return new EstudianteDetachedCriteria(createCriteria("ORM_Estudiante"));
	}
	
	public Curso uniqueCurso(PersistentSession session) {
		return (Curso) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Curso[] listCurso(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Curso[]) list.toArray(new Curso[list.size()]);
	}
}

