/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class NotaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression estudiante_id_fkId;
	public final AssociationExpression estudiante_id_fk;
	public final IntegerExpression asignaturaid_pkId;
	public final AssociationExpression asignaturaid_pk;
	public final FloatExpression nota;
	
	public NotaDetachedCriteria() {
		super(libropackage.Nota.class, libropackage.NotaCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		estudiante_id_fkId = new IntegerExpression("estudiante_id_fk.id_pk", this.getDetachedCriteria());
		estudiante_id_fk = new AssociationExpression("estudiante_id_fk", this.getDetachedCriteria());
		asignaturaid_pkId = new IntegerExpression("asignaturaid_pk.id_pk", this.getDetachedCriteria());
		asignaturaid_pk = new AssociationExpression("asignaturaid_pk", this.getDetachedCriteria());
		nota = new FloatExpression("nota", this.getDetachedCriteria());
	}
	
	public NotaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, libropackage.NotaCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		estudiante_id_fkId = new IntegerExpression("estudiante_id_fk.id_pk", this.getDetachedCriteria());
		estudiante_id_fk = new AssociationExpression("estudiante_id_fk", this.getDetachedCriteria());
		asignaturaid_pkId = new IntegerExpression("asignaturaid_pk.id_pk", this.getDetachedCriteria());
		asignaturaid_pk = new AssociationExpression("asignaturaid_pk", this.getDetachedCriteria());
		nota = new FloatExpression("nota", this.getDetachedCriteria());
	}
	
	public EstudianteDetachedCriteria createEstudiante_id_fkCriteria() {
		return new EstudianteDetachedCriteria(createCriteria("estudiante_id_fk"));
	}
	
	public AsignaturaDetachedCriteria createAsignaturaid_pkCriteria() {
		return new AsignaturaDetachedCriteria(createCriteria("asignaturaid_pk"));
	}
	
	public Nota uniqueNota(PersistentSession session) {
		return (Nota) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Nota[] listNota(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Nota[]) list.toArray(new Nota[list.size()]);
	}
}

