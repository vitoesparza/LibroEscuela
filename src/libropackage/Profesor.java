/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Profesor {
	public Profesor() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == libropackage.ORMConstants.KEY_PROFESOR_ANOTACIONES) {
			return ORM_anotaciones;
		}
		else if (key == libropackage.ORMConstants.KEY_PROFESOR_ASIGNATURA) {
			return ORM_asignatura;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id_pk;
	
	private String nombre;
	
	private String apellido;
	
	private String rut;
	
	private java.util.Set ORM_anotaciones = new java.util.HashSet();
	
	private java.util.Set ORM_asignatura = new java.util.HashSet();
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setApellido(String value) {
		this.apellido = value;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setRut(String value) {
		this.rut = value;
	}
	
	public String getRut() {
		return rut;
	}
	
	private void setORM_Anotaciones(java.util.Set value) {
		this.ORM_anotaciones = value;
	}
	
	private java.util.Set getORM_Anotaciones() {
		return ORM_anotaciones;
	}
	
	public final libropackage.AnotacionesSetCollection anotaciones = new libropackage.AnotacionesSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_PROFESOR_ANOTACIONES, libropackage.ORMConstants.KEY_ANOTACIONES_PROFESOR_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Asignatura(java.util.Set value) {
		this.ORM_asignatura = value;
	}
	
	private java.util.Set getORM_Asignatura() {
		return ORM_asignatura;
	}
	
	public final libropackage.AsignaturaSetCollection asignatura = new libropackage.AsignaturaSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_PROFESOR_ASIGNATURA, libropackage.ORMConstants.KEY_ASIGNATURA_PROFESORID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
