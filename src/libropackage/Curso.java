/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Curso {
	public Curso() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == libropackage.ORMConstants.KEY_CURSO_ASIGNATURA) {
			return ORM_asignatura;
		}
		else if (key == libropackage.ORMConstants.KEY_CURSO_ESTUDIANTE) {
			return ORM_estudiante;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_CURSO_INSTITUCION_ID_FK) {
			this.institucion_id_fk = (libropackage.Institucion) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Institucion institucion_id_fk;
	
	private String letra;
	
	private Integer nivel;
	
	private java.util.Set ORM_asignatura = new java.util.HashSet();
	
	private java.util.Set ORM_estudiante = new java.util.HashSet();
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setLetra(String value) {
		this.letra = value;
	}
	
	public String getLetra() {
		return letra;
	}
	
	public void setNivel(int value) {
		setNivel(new Integer(value));
	}
	
	public void setNivel(Integer value) {
		this.nivel = value;
	}
	
	public Integer getNivel() {
		return nivel;
	}
	
	public void setInstitucion_id_fk(libropackage.Institucion value) {
		if (institucion_id_fk != null) {
			institucion_id_fk.curso.remove(this);
		}
		if (value != null) {
			value.curso.add(this);
		}
	}
	
	public libropackage.Institucion getInstitucion_id_fk() {
		return institucion_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Institucion_id_fk(libropackage.Institucion value) {
		this.institucion_id_fk = value;
	}
	
	private libropackage.Institucion getORM_Institucion_id_fk() {
		return institucion_id_fk;
	}
	
	private void setORM_Asignatura(java.util.Set value) {
		this.ORM_asignatura = value;
	}
	
	public java.util.Set getORM_Asignatura() {
		return ORM_asignatura;
	}
	
	public final libropackage.AsignaturaSetCollection asignatura = new libropackage.AsignaturaSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_CURSO_ASIGNATURA, libropackage.ORMConstants.KEY_ASIGNATURA_CURSO_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Estudiante(java.util.Set value) {
		this.ORM_estudiante = value;
	}
	
	private java.util.Set getORM_Estudiante() {
		return ORM_estudiante;
	}
	
	public final libropackage.EstudianteSetCollection estudiante = new libropackage.EstudianteSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_CURSO_ESTUDIANTE, libropackage.ORMConstants.KEY_ESTUDIANTE_CURSOID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
