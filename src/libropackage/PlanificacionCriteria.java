/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PlanificacionCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression asignatura_id_fkId;
	public final AssociationExpression asignatura_id_fk;
	public final StringExpression actividad;
	
	public PlanificacionCriteria(Criteria criteria) {
		super(criteria);
		id_pk = new IntegerExpression("id_pk", this);
		asignatura_id_fkId = new IntegerExpression("asignatura_id_fk.id_pk", this);
		asignatura_id_fk = new AssociationExpression("asignatura_id_fk", this);
		actividad = new StringExpression("actividad", this);
	}
	
	public PlanificacionCriteria(PersistentSession session) {
		this(session.createCriteria(Planificacion.class));
	}
	
	public PlanificacionCriteria() throws PersistentException {
		this(libropackage.LibroClasePersistentManager.instance().getSession());
	}
	
	public AsignaturaCriteria createAsignatura_id_fkCriteria() {
		return new AsignaturaCriteria(createCriteria("asignatura_id_fk"));
	}
	
	public Planificacion uniquePlanificacion() {
		return (Planificacion) super.uniqueResult();
	}
	
	public Planificacion[] listPlanificacion() {
		java.util.List list = super.list();
		return (Planificacion[]) list.toArray(new Planificacion[list.size()]);
	}
}

