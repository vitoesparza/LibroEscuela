/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Estudiante {
	public Estudiante() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == libropackage.ORMConstants.KEY_ESTUDIANTE_NOTA) {
			return ORM_nota;
		}
		else if (key == libropackage.ORMConstants.KEY_ESTUDIANTE_ANOTACIONES) {
			return ORM_anotaciones;
		}
		else if (key == libropackage.ORMConstants.KEY_ESTUDIANTE_ASISTENCIA) {
			return ORM_asistencia;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_ESTUDIANTE_APODERADO_ID_FK) {
			this.apoderado_id_fk = (libropackage.Apoderado) owner;
		}
		
		else if (key == libropackage.ORMConstants.KEY_ESTUDIANTE_CURSOID_FK) {
			this.cursoid_fk = (libropackage.Curso) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Apoderado apoderado_id_fk;
	
	private libropackage.Curso cursoid_fk;
	
	private String nombre;
	
	private String apellido;
	
	private String rut;
	
	private java.util.Set ORM_nota = new java.util.HashSet();
	
	private java.util.Set ORM_anotaciones = new java.util.HashSet();
	
	private java.util.Set ORM_asistencia = new java.util.HashSet();
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setApellido(String value) {
		this.apellido = value;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setRut(String value) {
		this.rut = value;
	}
	
	public String getRut() {
		return rut;
	}
	
	public void setApoderado_id_fk(libropackage.Apoderado value) {
		if (apoderado_id_fk != null) {
			apoderado_id_fk.estudiante.remove(this);
		}
		if (value != null) {
			value.estudiante.add(this);
		}
	}
	
	public libropackage.Apoderado getApoderado_id_fk() {
		return apoderado_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Apoderado_id_fk(libropackage.Apoderado value) {
		this.apoderado_id_fk = value;
	}
	
	private libropackage.Apoderado getORM_Apoderado_id_fk() {
		return apoderado_id_fk;
	}
	
	public void setCursoid_fk(libropackage.Curso value) {
		if (cursoid_fk != null) {
			cursoid_fk.estudiante.remove(this);
		}
		if (value != null) {
			value.estudiante.add(this);
		}
	}
	
	public libropackage.Curso getCursoid_fk() {
		return cursoid_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Cursoid_fk(libropackage.Curso value) {
		this.cursoid_fk = value;
	}
	
	private libropackage.Curso getORM_Cursoid_fk() {
		return cursoid_fk;
	}
	
	private void setORM_Nota(java.util.Set value) {
		this.ORM_nota = value;
	}
	
	private java.util.Set getORM_Nota() {
		return ORM_nota;
	}
	
	public final libropackage.NotaSetCollection nota = new libropackage.NotaSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_ESTUDIANTE_NOTA, libropackage.ORMConstants.KEY_NOTA_ESTUDIANTE_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Anotaciones(java.util.Set value) {
		this.ORM_anotaciones = value;
	}
	
	private java.util.Set getORM_Anotaciones() {
		return ORM_anotaciones;
	}
	
	public final libropackage.AnotacionesSetCollection anotaciones = new libropackage.AnotacionesSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_ESTUDIANTE_ANOTACIONES, libropackage.ORMConstants.KEY_ANOTACIONES_ESTUDIANTE_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Asistencia(java.util.Set value) {
		this.ORM_asistencia = value;
	}
	
	private java.util.Set getORM_Asistencia() {
		return ORM_asistencia;
	}
	
	public final libropackage.AsistenciaSetCollection asistencia = new libropackage.AsistenciaSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_ESTUDIANTE_ASISTENCIA, libropackage.ORMConstants.KEY_ASISTENCIA_ESTUDIANTE_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
