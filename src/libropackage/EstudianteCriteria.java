/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EstudianteCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression apoderado_id_fkId;
	public final AssociationExpression apoderado_id_fk;
	public final IntegerExpression cursoid_fkId;
	public final AssociationExpression cursoid_fk;
	public final StringExpression nombre;
	public final StringExpression apellido;
	public final StringExpression rut;
	public final CollectionExpression nota;
	public final CollectionExpression anotaciones;
	public final CollectionExpression asistencia;
	
	public EstudianteCriteria(Criteria criteria) {
		super(criteria);
		id_pk = new IntegerExpression("id_pk", this);
		apoderado_id_fkId = new IntegerExpression("apoderado_id_fk.id_pk", this);
		apoderado_id_fk = new AssociationExpression("apoderado_id_fk", this);
		cursoid_fkId = new IntegerExpression("cursoid_fk.id_pk", this);
		cursoid_fk = new AssociationExpression("cursoid_fk", this);
		nombre = new StringExpression("nombre", this);
		apellido = new StringExpression("apellido", this);
		rut = new StringExpression("rut", this);
		nota = new CollectionExpression("ORM_Nota", this);
		anotaciones = new CollectionExpression("ORM_Anotaciones", this);
		asistencia = new CollectionExpression("ORM_Asistencia", this);
	}
	
	public EstudianteCriteria(PersistentSession session) {
		this(session.createCriteria(Estudiante.class));
	}
	
	public EstudianteCriteria() throws PersistentException {
		this(libropackage.LibroClasePersistentManager.instance().getSession());
	}
	
	public ApoderadoCriteria createApoderado_id_fkCriteria() {
		return new ApoderadoCriteria(createCriteria("apoderado_id_fk"));
	}
	
	public CursoCriteria createCursoid_fkCriteria() {
		return new CursoCriteria(createCriteria("cursoid_fk"));
	}
	
	public NotaCriteria createNotaCriteria() {
		return new NotaCriteria(createCriteria("ORM_Nota"));
	}
	
	public AnotacionesCriteria createAnotacionesCriteria() {
		return new AnotacionesCriteria(createCriteria("ORM_Anotaciones"));
	}
	
	public AsistenciaCriteria createAsistenciaCriteria() {
		return new AsistenciaCriteria(createCriteria("ORM_Asistencia"));
	}
	
	public Estudiante uniqueEstudiante() {
		return (Estudiante) super.uniqueResult();
	}
	
	public Estudiante[] listEstudiante() {
		java.util.List list = super.list();
		return (Estudiante[]) list.toArray(new Estudiante[list.size()]);
	}
}

