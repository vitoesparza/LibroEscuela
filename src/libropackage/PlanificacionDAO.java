/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class PlanificacionDAO {
	public static Planificacion loadPlanificacionByORMID(int id_pk) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return loadPlanificacionByORMID(session, id_pk);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion getPlanificacionByORMID(int id_pk) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return getPlanificacionByORMID(session, id_pk);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByORMID(int id_pk, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return loadPlanificacionByORMID(session, id_pk, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion getPlanificacionByORMID(int id_pk, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return getPlanificacionByORMID(session, id_pk, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByORMID(PersistentSession session, int id_pk) throws PersistentException {
		try {
			return (Planificacion) session.load(libropackage.Planificacion.class, new Integer(id_pk));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion getPlanificacionByORMID(PersistentSession session, int id_pk) throws PersistentException {
		try {
			return (Planificacion) session.get(libropackage.Planificacion.class, new Integer(id_pk));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByORMID(PersistentSession session, int id_pk, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Planificacion) session.load(libropackage.Planificacion.class, new Integer(id_pk), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion getPlanificacionByORMID(PersistentSession session, int id_pk, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Planificacion) session.get(libropackage.Planificacion.class, new Integer(id_pk), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPlanificacion(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return queryPlanificacion(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPlanificacion(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return queryPlanificacion(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion[] listPlanificacionByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return listPlanificacionByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion[] listPlanificacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return listPlanificacionByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPlanificacion(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From libropackage.Planificacion as Planificacion");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryPlanificacion(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From libropackage.Planificacion as Planificacion");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Planificacion", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion[] listPlanificacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryPlanificacion(session, condition, orderBy);
			return (Planificacion[]) list.toArray(new Planificacion[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion[] listPlanificacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryPlanificacion(session, condition, orderBy, lockMode);
			return (Planificacion[]) list.toArray(new Planificacion[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return loadPlanificacionByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return loadPlanificacionByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Planificacion[] planificacions = listPlanificacionByQuery(session, condition, orderBy);
		if (planificacions != null && planificacions.length > 0)
			return planificacions[0];
		else
			return null;
	}
	
	public static Planificacion loadPlanificacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Planificacion[] planificacions = listPlanificacionByQuery(session, condition, orderBy, lockMode);
		if (planificacions != null && planificacions.length > 0)
			return planificacions[0];
		else
			return null;
	}
	
	public static java.util.Iterator iteratePlanificacionByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return iteratePlanificacionByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePlanificacionByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = libropackage.LibroClasePersistentManager.instance().getSession();
			return iteratePlanificacionByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePlanificacionByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From libropackage.Planificacion as Planificacion");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePlanificacionByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From libropackage.Planificacion as Planificacion");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Planificacion", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion createPlanificacion() {
		return new libropackage.Planificacion();
	}
	
	public static boolean save(libropackage.Planificacion planificacion) throws PersistentException {
		try {
			libropackage.LibroClasePersistentManager.instance().saveObject(planificacion);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(libropackage.Planificacion planificacion) throws PersistentException {
		try {
			libropackage.LibroClasePersistentManager.instance().deleteObject(planificacion);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(libropackage.Planificacion planificacion)throws PersistentException {
		try {
			if (planificacion.getAsignatura_id_fk() != null) {
				planificacion.getAsignatura_id_fk().planificacion.remove(planificacion);
			}
			
			return delete(planificacion);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(libropackage.Planificacion planificacion, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (planificacion.getAsignatura_id_fk() != null) {
				planificacion.getAsignatura_id_fk().planificacion.remove(planificacion);
			}
			
			try {
				session.delete(planificacion);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(libropackage.Planificacion planificacion) throws PersistentException {
		try {
			libropackage.LibroClasePersistentManager.instance().getSession().refresh(planificacion);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(libropackage.Planificacion planificacion) throws PersistentException {
		try {
			libropackage.LibroClasePersistentManager.instance().getSession().evict(planificacion);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Planificacion loadPlanificacionByCriteria(PlanificacionCriteria planificacionCriteria) {
		Planificacion[] planificacions = listPlanificacionByCriteria(planificacionCriteria);
		if(planificacions == null || planificacions.length == 0) {
			return null;
		}
		return planificacions[0];
	}
	
	public static Planificacion[] listPlanificacionByCriteria(PlanificacionCriteria planificacionCriteria) {
		return planificacionCriteria.listPlanificacion();
	}
}
