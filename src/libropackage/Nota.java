/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Nota {
	public Nota() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_NOTA_ESTUDIANTE_ID_FK) {
			this.estudiante_id_fk = (libropackage.Estudiante) owner;
		}
		
		else if (key == libropackage.ORMConstants.KEY_NOTA_ASIGNATURAID_PK) {
			this.asignaturaid_pk = (libropackage.Asignatura) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Estudiante estudiante_id_fk;
	
	private libropackage.Asignatura asignaturaid_pk;
	
	private Float nota;
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setNota(float value) {
		setNota(new Float(value));
	}
	
	public void setNota(Float value) {
		this.nota = value;
	}
	
	public Float getNota() {
		return nota;
	}
	
	public void setEstudiante_id_fk(libropackage.Estudiante value) {
		if (estudiante_id_fk != null) {
			estudiante_id_fk.nota.remove(this);
		}
		if (value != null) {
			value.nota.add(this);
		}
	}
	
	public libropackage.Estudiante getEstudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Estudiante_id_fk(libropackage.Estudiante value) {
		this.estudiante_id_fk = value;
	}
	
	private libropackage.Estudiante getORM_Estudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	public void setAsignaturaid_pk(libropackage.Asignatura value) {
		if (asignaturaid_pk != null) {
			asignaturaid_pk.nota.remove(this);
		}
		if (value != null) {
			value.nota.add(this);
		}
	}
	
	public libropackage.Asignatura getAsignaturaid_pk() {
		return asignaturaid_pk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Asignaturaid_pk(libropackage.Asignatura value) {
		this.asignaturaid_pk = value;
	}
	
	private libropackage.Asignatura getORM_Asignaturaid_pk() {
		return asignaturaid_pk;
	}
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
