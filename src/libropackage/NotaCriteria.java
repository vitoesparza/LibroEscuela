/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class NotaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression estudiante_id_fkId;
	public final AssociationExpression estudiante_id_fk;
	public final IntegerExpression asignaturaid_pkId;
	public final AssociationExpression asignaturaid_pk;
	public final FloatExpression nota;
	
	public NotaCriteria(Criteria criteria) {
		super(criteria);
		id_pk = new IntegerExpression("id_pk", this);
		estudiante_id_fkId = new IntegerExpression("estudiante_id_fk.id_pk", this);
		estudiante_id_fk = new AssociationExpression("estudiante_id_fk", this);
		asignaturaid_pkId = new IntegerExpression("asignaturaid_pk.id_pk", this);
		asignaturaid_pk = new AssociationExpression("asignaturaid_pk", this);
		nota = new FloatExpression("nota", this);
	}
	
	public NotaCriteria(PersistentSession session) {
		this(session.createCriteria(Nota.class));
	}
	
	public NotaCriteria() throws PersistentException {
		this(libropackage.LibroClasePersistentManager.instance().getSession());
	}
	
	public EstudianteCriteria createEstudiante_id_fkCriteria() {
		return new EstudianteCriteria(createCriteria("estudiante_id_fk"));
	}
	
	public AsignaturaCriteria createAsignaturaid_pkCriteria() {
		return new AsignaturaCriteria(createCriteria("asignaturaid_pk"));
	}
	
	public Nota uniqueNota() {
		return (Nota) super.uniqueResult();
	}
	
	public Nota[] listNota() {
		java.util.List list = super.list();
		return (Nota[]) list.toArray(new Nota[list.size()]);
	}
}

