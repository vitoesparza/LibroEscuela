/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PlanificacionDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression asignatura_id_fkId;
	public final AssociationExpression asignatura_id_fk;
	public final StringExpression actividad;
	
	public PlanificacionDetachedCriteria() {
		super(libropackage.Planificacion.class, libropackage.PlanificacionCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		asignatura_id_fkId = new IntegerExpression("asignatura_id_fk.id_pk", this.getDetachedCriteria());
		asignatura_id_fk = new AssociationExpression("asignatura_id_fk", this.getDetachedCriteria());
		actividad = new StringExpression("actividad", this.getDetachedCriteria());
	}
	
	public PlanificacionDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, libropackage.PlanificacionCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		asignatura_id_fkId = new IntegerExpression("asignatura_id_fk.id_pk", this.getDetachedCriteria());
		asignatura_id_fk = new AssociationExpression("asignatura_id_fk", this.getDetachedCriteria());
		actividad = new StringExpression("actividad", this.getDetachedCriteria());
	}
	
	public AsignaturaDetachedCriteria createAsignatura_id_fkCriteria() {
		return new AsignaturaDetachedCriteria(createCriteria("asignatura_id_fk"));
	}
	
	public Planificacion uniquePlanificacion(PersistentSession session) {
		return (Planificacion) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Planificacion[] listPlanificacion(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Planificacion[]) list.toArray(new Planificacion[list.size()]);
	}
}

