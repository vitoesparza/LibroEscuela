/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Anotaciones {
	public Anotaciones() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_ANOTACIONES_ESTUDIANTE_ID_FK) {
			this.estudiante_id_fk = (libropackage.Estudiante) owner;
		}
		
		else if (key == libropackage.ORMConstants.KEY_ANOTACIONES_PROFESOR_ID_FK) {
			this.profesor_id_fk = (libropackage.Profesor) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Estudiante estudiante_id_fk;
	
	private libropackage.Profesor profesor_id_fk;
	
	private String anotacion;
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setAnotacion(String value) {
		this.anotacion = value;
	}
	
	public String getAnotacion() {
		return anotacion;
	}
	
	public void setEstudiante_id_fk(libropackage.Estudiante value) {
		if (estudiante_id_fk != null) {
			estudiante_id_fk.anotaciones.remove(this);
		}
		if (value != null) {
			value.anotaciones.add(this);
		}
	}
	
	public libropackage.Estudiante getEstudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Estudiante_id_fk(libropackage.Estudiante value) {
		this.estudiante_id_fk = value;
	}
	
	private libropackage.Estudiante getORM_Estudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	public void setProfesor_id_fk(libropackage.Profesor value) {
		if (profesor_id_fk != null) {
			profesor_id_fk.anotaciones.remove(this);
		}
		if (value != null) {
			value.anotaciones.add(this);
		}
	}
	
	public libropackage.Profesor getProfesor_id_fk() {
		return profesor_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Profesor_id_fk(libropackage.Profesor value) {
		this.profesor_id_fk = value;
	}
	
	private libropackage.Profesor getORM_Profesor_id_fk() {
		return profesor_id_fk;
	}
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
