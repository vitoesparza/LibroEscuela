/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EstudianteDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression apoderado_id_fkId;
	public final AssociationExpression apoderado_id_fk;
	public final IntegerExpression cursoid_fkId;
	public final AssociationExpression cursoid_fk;
	public final StringExpression nombre;
	public final StringExpression apellido;
	public final StringExpression rut;
	public final CollectionExpression nota;
	public final CollectionExpression anotaciones;
	public final CollectionExpression asistencia;
	
	public EstudianteDetachedCriteria() {
		super(libropackage.Estudiante.class, libropackage.EstudianteCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		apoderado_id_fkId = new IntegerExpression("apoderado_id_fk.id_pk", this.getDetachedCriteria());
		apoderado_id_fk = new AssociationExpression("apoderado_id_fk", this.getDetachedCriteria());
		cursoid_fkId = new IntegerExpression("cursoid_fk.id_pk", this.getDetachedCriteria());
		cursoid_fk = new AssociationExpression("cursoid_fk", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		apellido = new StringExpression("apellido", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		nota = new CollectionExpression("ORM_Nota", this.getDetachedCriteria());
		anotaciones = new CollectionExpression("ORM_Anotaciones", this.getDetachedCriteria());
		asistencia = new CollectionExpression("ORM_Asistencia", this.getDetachedCriteria());
	}
	
	public EstudianteDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, libropackage.EstudianteCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		apoderado_id_fkId = new IntegerExpression("apoderado_id_fk.id_pk", this.getDetachedCriteria());
		apoderado_id_fk = new AssociationExpression("apoderado_id_fk", this.getDetachedCriteria());
		cursoid_fkId = new IntegerExpression("cursoid_fk.id_pk", this.getDetachedCriteria());
		cursoid_fk = new AssociationExpression("cursoid_fk", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		apellido = new StringExpression("apellido", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		nota = new CollectionExpression("ORM_Nota", this.getDetachedCriteria());
		anotaciones = new CollectionExpression("ORM_Anotaciones", this.getDetachedCriteria());
		asistencia = new CollectionExpression("ORM_Asistencia", this.getDetachedCriteria());
	}
	
	public ApoderadoDetachedCriteria createApoderado_id_fkCriteria() {
		return new ApoderadoDetachedCriteria(createCriteria("apoderado_id_fk"));
	}
	
	public CursoDetachedCriteria createCursoid_fkCriteria() {
		return new CursoDetachedCriteria(createCriteria("cursoid_fk"));
	}
	
	public NotaDetachedCriteria createNotaCriteria() {
		return new NotaDetachedCriteria(createCriteria("ORM_Nota"));
	}
	
	public AnotacionesDetachedCriteria createAnotacionesCriteria() {
		return new AnotacionesDetachedCriteria(createCriteria("ORM_Anotaciones"));
	}
	
	public AsistenciaDetachedCriteria createAsistenciaCriteria() {
		return new AsistenciaDetachedCriteria(createCriteria("ORM_Asistencia"));
	}
	
	public Estudiante uniqueEstudiante(PersistentSession session) {
		return (Estudiante) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Estudiante[] listEstudiante(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Estudiante[]) list.toArray(new Estudiante[list.size()]);
	}
}

