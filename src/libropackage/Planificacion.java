/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Planificacion {
	public Planificacion() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_PLANIFICACION_ASIGNATURA_ID_FK) {
			this.asignatura_id_fk = (libropackage.Asignatura) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Asignatura asignatura_id_fk;
	
	private String actividad;
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setActividad(String value) {
		this.actividad = value;
	}
	
	public String getActividad() {
		return actividad;
	}
	
	public void setAsignatura_id_fk(libropackage.Asignatura value) {
		if (asignatura_id_fk != null) {
			asignatura_id_fk.planificacion.remove(this);
		}
		if (value != null) {
			value.planificacion.add(this);
		}
	}
	
	public libropackage.Asignatura getAsignatura_id_fk() {
		return asignatura_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Asignatura_id_fk(libropackage.Asignatura value) {
		this.asignatura_id_fk = value;
	}
	
	private libropackage.Asignatura getORM_Asignatura_id_fk() {
		return asignatura_id_fk;
	}
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
