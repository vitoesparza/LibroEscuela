/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Asistencia {
	public Asistencia() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_ASISTENCIA_ESTUDIANTE_ID_FK) {
			this.estudiante_id_fk = (libropackage.Estudiante) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Estudiante estudiante_id_fk;
	
	private Integer registro;
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setRegistro(int value) {
		setRegistro(new Integer(value));
	}
	
	public void setRegistro(Integer value) {
		this.registro = value;
	}
	
	public Integer getRegistro() {
		return registro;
	}
	
	public void setEstudiante_id_fk(libropackage.Estudiante value) {
		if (estudiante_id_fk != null) {
			estudiante_id_fk.asistencia.remove(this);
		}
		if (value != null) {
			value.asistencia.add(this);
		}
	}
	
	public libropackage.Estudiante getEstudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Estudiante_id_fk(libropackage.Estudiante value) {
		this.estudiante_id_fk = value;
	}
	
	private libropackage.Estudiante getORM_Estudiante_id_fk() {
		return estudiante_id_fk;
	}
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
