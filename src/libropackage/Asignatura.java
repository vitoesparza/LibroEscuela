/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Asignatura {
	public Asignatura() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == libropackage.ORMConstants.KEY_ASIGNATURA_PLANIFICACION) {
			return ORM_planificacion;
		}
		else if (key == libropackage.ORMConstants.KEY_ASIGNATURA_NOTA) {
			return ORM_nota;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == libropackage.ORMConstants.KEY_ASIGNATURA_CURSO_ID_FK) {
			this.curso_id_fk = (libropackage.Curso) owner;
		}
		
		else if (key == libropackage.ORMConstants.KEY_ASIGNATURA_PROFESORID_FK) {
			this.profesorid_fk = (libropackage.Profesor) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id_pk;
	
	private libropackage.Curso curso_id_fk;
	
	private libropackage.Profesor profesorid_fk;
	
	private String materia;
	
	private java.util.Set ORM_planificacion = new java.util.HashSet();
	
	private java.util.Set ORM_nota = new java.util.HashSet();
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setMateria(String value) {
		this.materia = value;
	}
	
	public String getMateria() {
		return materia;
	}
	
	public void setCurso_id_fk(libropackage.Curso value) {
		if (curso_id_fk != null) {
			curso_id_fk.asignatura.remove(this);
		}
		if (value != null) {
			value.asignatura.add(this);
		}
	}
	
	public libropackage.Curso getCurso_id_fk() {
		return curso_id_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Curso_id_fk(libropackage.Curso value) {
		this.curso_id_fk = value;
	}
	
	private libropackage.Curso getORM_Curso_id_fk() {
		return curso_id_fk;
	}
	
	public void setProfesorid_fk(libropackage.Profesor value) {
		if (profesorid_fk != null) {
			profesorid_fk.asignatura.remove(this);
		}
		if (value != null) {
			value.asignatura.add(this);
		}
	}
	
	public libropackage.Profesor getProfesorid_fk() {
		return profesorid_fk;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Profesorid_fk(libropackage.Profesor value) {
		this.profesorid_fk = value;
	}
	
	private libropackage.Profesor getORM_Profesorid_fk() {
		return profesorid_fk;
	}
	
	private void setORM_Planificacion(java.util.Set value) {
		this.ORM_planificacion = value;
	}
	
	private java.util.Set getORM_Planificacion() {
		return ORM_planificacion;
	}
	
	public final libropackage.PlanificacionSetCollection planificacion = new libropackage.PlanificacionSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_ASIGNATURA_PLANIFICACION, libropackage.ORMConstants.KEY_PLANIFICACION_ASIGNATURA_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Nota(java.util.Set value) {
		this.ORM_nota = value;
	}
	
	private java.util.Set getORM_Nota() {
		return ORM_nota;
	}
	
	public final libropackage.NotaSetCollection nota = new libropackage.NotaSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_ASIGNATURA_NOTA, libropackage.ORMConstants.KEY_NOTA_ASIGNATURAID_PK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
