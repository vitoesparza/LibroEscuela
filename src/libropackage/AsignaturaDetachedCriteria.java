/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsignaturaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression curso_id_fkId;
	public final AssociationExpression curso_id_fk;
	public final IntegerExpression profesorid_fkId;
	public final AssociationExpression profesorid_fk;
	public final StringExpression materia;
	public final CollectionExpression planificacion;
	public final CollectionExpression nota;
	
	public AsignaturaDetachedCriteria() {
		super(libropackage.Asignatura.class, libropackage.AsignaturaCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		curso_id_fkId = new IntegerExpression("curso_id_fk.id_pk", this.getDetachedCriteria());
		curso_id_fk = new AssociationExpression("curso_id_fk", this.getDetachedCriteria());
		profesorid_fkId = new IntegerExpression("profesorid_fk.id_pk", this.getDetachedCriteria());
		profesorid_fk = new AssociationExpression("profesorid_fk", this.getDetachedCriteria());
		materia = new StringExpression("materia", this.getDetachedCriteria());
		planificacion = new CollectionExpression("ORM_Planificacion", this.getDetachedCriteria());
		nota = new CollectionExpression("ORM_Nota", this.getDetachedCriteria());
	}
	
	public AsignaturaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, libropackage.AsignaturaCriteria.class);
		id_pk = new IntegerExpression("id_pk", this.getDetachedCriteria());
		curso_id_fkId = new IntegerExpression("curso_id_fk.id_pk", this.getDetachedCriteria());
		curso_id_fk = new AssociationExpression("curso_id_fk", this.getDetachedCriteria());
		profesorid_fkId = new IntegerExpression("profesorid_fk.id_pk", this.getDetachedCriteria());
		profesorid_fk = new AssociationExpression("profesorid_fk", this.getDetachedCriteria());
		materia = new StringExpression("materia", this.getDetachedCriteria());
		planificacion = new CollectionExpression("ORM_Planificacion", this.getDetachedCriteria());
		nota = new CollectionExpression("ORM_Nota", this.getDetachedCriteria());
	}
	
	public CursoDetachedCriteria createCurso_id_fkCriteria() {
		return new CursoDetachedCriteria(createCriteria("curso_id_fk"));
	}
	
	public ProfesorDetachedCriteria createProfesorid_fkCriteria() {
		return new ProfesorDetachedCriteria(createCriteria("profesorid_fk"));
	}
	
	public PlanificacionDetachedCriteria createPlanificacionCriteria() {
		return new PlanificacionDetachedCriteria(createCriteria("ORM_Planificacion"));
	}
	
	public NotaDetachedCriteria createNotaCriteria() {
		return new NotaDetachedCriteria(createCriteria("ORM_Nota"));
	}
	
	public Asignatura uniqueAsignatura(PersistentSession session) {
		return (Asignatura) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Asignatura[] listAsignatura(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Asignatura[]) list.toArray(new Asignatura[list.size()]);
	}
}

