/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Apoderado {
	public Apoderado() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == libropackage.ORMConstants.KEY_APODERADO_ESTUDIANTE) {
			return ORM_estudiante;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id_pk;
	
	private String nombre;
	
	private String apellido;
	
	private String rut;
	
	private java.util.Set ORM_estudiante = new java.util.HashSet();
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setApellido(String value) {
		this.apellido = value;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setRut(String value) {
		this.rut = value;
	}
	
	public String getRut() {
		return rut;
	}
	
	private void setORM_Estudiante(java.util.Set value) {
		this.ORM_estudiante = value;
	}
	
	private java.util.Set getORM_Estudiante() {
		return ORM_estudiante;
	}
	
	public final libropackage.EstudianteSetCollection estudiante = new libropackage.EstudianteSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_APODERADO_ESTUDIANTE, libropackage.ORMConstants.KEY_ESTUDIANTE_APODERADO_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
