/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class AsignaturaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id_pk;
	public final IntegerExpression curso_id_fkId;
	public final AssociationExpression curso_id_fk;
	public final IntegerExpression profesorid_fkId;
	public final AssociationExpression profesorid_fk;
	public final StringExpression materia;
	public final CollectionExpression planificacion;
	public final CollectionExpression nota;
	
	public AsignaturaCriteria(Criteria criteria) {
		super(criteria);
		id_pk = new IntegerExpression("id_pk", this);
		curso_id_fkId = new IntegerExpression("curso_id_fk.id_pk", this);
		curso_id_fk = new AssociationExpression("curso_id_fk", this);
		profesorid_fkId = new IntegerExpression("profesorid_fk.id_pk", this);
		profesorid_fk = new AssociationExpression("profesorid_fk", this);
		materia = new StringExpression("materia", this);
		planificacion = new CollectionExpression("ORM_Planificacion", this);
		nota = new CollectionExpression("ORM_Nota", this);
	}
	
	public AsignaturaCriteria(PersistentSession session) {
		this(session.createCriteria(Asignatura.class));
	}
	
	public AsignaturaCriteria() throws PersistentException {
		this(libropackage.LibroClasePersistentManager.instance().getSession());
	}
	
	public CursoCriteria createCurso_id_fkCriteria() {
		return new CursoCriteria(createCriteria("curso_id_fk"));
	}
	
	public ProfesorCriteria createProfesorid_fkCriteria() {
		return new ProfesorCriteria(createCriteria("profesorid_fk"));
	}
	
	public PlanificacionCriteria createPlanificacionCriteria() {
		return new PlanificacionCriteria(createCriteria("ORM_Planificacion"));
	}
	
	public NotaCriteria createNotaCriteria() {
		return new NotaCriteria(createCriteria("ORM_Nota"));
	}
	
	public Asignatura uniqueAsignatura() {
		return (Asignatura) super.uniqueResult();
	}
	
	public Asignatura[] listAsignatura() {
		java.util.List list = super.list();
		return (Asignatura[]) list.toArray(new Asignatura[list.size()]);
	}
}

