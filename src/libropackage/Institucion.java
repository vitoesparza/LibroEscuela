/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package libropackage;

public class Institucion {
	public Institucion() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == libropackage.ORMConstants.KEY_INSTITUCION_CURSO) {
			return ORM_curso;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id_pk;
	
	private java.util.Set ORM_curso = new java.util.HashSet();
	
	private void setId_pk(int value) {
		this.id_pk = value;
	}
	
	public int getId_pk() {
		return id_pk;
	}
	
	public int getORMID() {
		return getId_pk();
	}
	
	private void setORM_Curso(java.util.Set value) {
		this.ORM_curso = value;
	}
	
	private java.util.Set getORM_Curso() {
		return ORM_curso;
	}
	
	public final libropackage.CursoSetCollection curso = new libropackage.CursoSetCollection(this, _ormAdapter, libropackage.ORMConstants.KEY_INSTITUCION_CURSO, libropackage.ORMConstants.KEY_CURSO_INSTITUCION_ID_FK, libropackage.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId_pk());
	}
	
}
