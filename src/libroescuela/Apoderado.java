/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"nombre","apellido","rut","pupilo"})
public class Apoderado implements Comparable<Apoderado>{

    private String nombre;
    private String apellido;
    private String rut;
    private ArrayList<Estudiante> pupilo;

    public Apoderado() {
    }
    
    /**
     * 
     * @param nombre resive el nombre 
     * @param apellido resive el apellido
     * @param rut resive el rut
     */
	public Apoderado(String nombre, String apellido, String rut) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
    }
    
	/**
	 * 
	 * @return retorna los estudiantes que tiene el apoderado.
	 */
    @XmlElement(name="pupilo")
    public ArrayList<Estudiante> getPupilo() {
        return pupilo;
    }

    public void setPupilo(ArrayList<Estudiante> pupilo) {
        this.pupilo = pupilo;
    }
    
    /**
     * 
     * @return retorna el nombre del apoderado.
     */
    @XmlElement(name="nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    	/**
    	 * 
    	 * @return retorna el apellido del apoderado
    	 */
    @XmlElement(name="apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    /**
     * 
     * @return retorna el rut del apoderado
     */
    @XmlElement(name="rut")
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }
    
    /**
     * este es un compaarador para poder obtener los apoderados ordenados por nombre.
     */
	@Override
	public int compareTo(Apoderado a) {
		// TODO Auto-generated method stub
		return this.nombre.compareTo(a.getNombre());
	}

}
