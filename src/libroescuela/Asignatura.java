/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"materia", "plan"})
public class Asignatura {

    private String materia;
    private ArrayList<Planificacion> plan;

    public Asignatura() {
    }
    
    /**
     *  se crea el objeto asignatura al cual se le insertar�
     * @param materia el string materia.
     */
    public Asignatura(String materia) {
        this.materia = materia;
    }
    /**
     * 
     * @return se obtiene el plan de la saginatura
     */
    @XmlElement(name = "plan")
    public ArrayList <Planificacion> getPlan() {
        return plan;
    }
    
    /**
     * con este m�todo se le agregan las planificaciones correspondientes a la asignatura
     * @param plan
     */
    public void setPlan(ArrayList<Planificacion> plan) {
        this.plan = plan;
    }
    
    /**
     * 
     * @return retorna el nombre de la materia
     */
    @XmlElement(name = "materia")
    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    @Override
    public String toString() {
        return materia;
    }

}
