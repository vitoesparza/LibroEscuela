/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * este es el curso que contrendea a los estudiantes y estar� vinculado a un profesor.
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"nivel","letra","estudiante","profesor"})
public class Curso {
    private int nivel;
    private String letra;
    private ArrayList<Estudiante> estudiante;
    private Profesor profesor;

    public Curso() {
    }
    
    /**
     * se crea el objeto curso
     * @param nivel el nivel del curso que va de 1ro a 8vo
     * @param letra la letra del curso que puede ser A o B
     * @param estudiante los estudiantes que estaran en el curso
     * @param profesor los profesores que estar�n en el curso
     */
    public Curso(int nivel, String letra, ArrayList<Estudiante> estudiante, 
            Profesor profesor) {
        this.nivel = nivel;
        this.letra = letra;
        this.estudiante = estudiante;
        this.profesor = profesor;
    }
    
    /**
     * 
     * @return rerona el nivel de 1 a 8
     */
    @XmlElement(name = "nivel")
    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    /**
     * 
     * @return retorna la letra del curso
     */
    @XmlElement(name = "letra")
    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }
    /**
     * 
     * @return retorna todos los estudiantes del curso
     */
    @XmlElement(name = "estudiante")
    public ArrayList<Estudiante> getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(ArrayList<Estudiante> estudiante) {
        this.estudiante = estudiante;
    }
    
    /**
     * 
     * @return retorna el profesor que corresponde al curso
     */
    @XmlElement(name = "profesor")
    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }
    
    
}
