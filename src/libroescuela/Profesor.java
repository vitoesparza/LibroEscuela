/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * este ser� la base de los cursos, los estudiantes
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"nombre", "apellido","rut", "asignatura","curso"})
public class Profesor {

    private String nombre;
    private String apellido;
    private String rut;
    private Asignatura asignatura;
    private Curso curso;

    public Profesor() {
    }
    	
    /**
     * para crear el objeto profesor
     * @param nombre ser� ingresado el nombre del profesor
     * @param apellido ser� ingresado el apellido del profesor
     * @param rut ser� ingresado el rut del profesor
     */
    public Profesor(String nombre, String apellido, String rut) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
    }
    
    /**
     * 
     * @return retorna el nombre del profesor
     */
    @XmlElement(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * 
     * @return rerona el apellido del profesor.
     */
    @XmlElement(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    /**
     * 
     * @return retorna el rut o dni del profesor
     */
    @XmlElement(name = "rut")
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }
    
    /**
     * 
     * @return retorna el curso al cual pertenece el profesor.
     */
    @XmlElement(name = "curso")
    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @Override
    public String toString() {
        return "Nombre : " + nombre + " " + apellido + ", Rut :" + rut ;
    }

}
