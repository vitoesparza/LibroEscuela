/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"nota","asignatura"})
public class Nota {
    private double nota;
    private Asignatura asignatura;
    public Nota() {
    }
    
    /**
     * se crear� una nota para el estudiante.
     * @param nota
     */
    public Nota(double nota) {
        this.nota = nota;
    }
    
    @XmlElement(name = "nota")
    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }
    @XmlElement(name = "asignatura")
    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    @Override
    public String toString() {
        return "Nota : " + nota + ", Asignatura : " + asignatura ;
    }
    
}
