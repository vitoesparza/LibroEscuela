/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder={"anotacion","profesor"})
public class Anotacion {

    private String anotacion;
    private Profesor profesor;

    public Anotacion() {
    }
    
    /**
     * crea el objeto apoderado al cual hay que pasarle
     * @param anotacion donde se resivira el string con la anotacion
     * @param profesor resive el objeto profesor quien es el que escribe la anotación.
     */
    public Anotacion(String anotacion, Profesor profesor) {
        this.anotacion = anotacion;
        this.profesor = profesor;
    }
    /**
     * 
     * @return retorna el profesor, por si necesita la informacion del profesor que ingresa la anotación.
     */
    @XmlElement(name="profesor")
    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }
    
    /**
     * 
     * @return retorna la anotación
     */
    @XmlElement(name="anotacion")
    public String getAnotacion() {
        return anotacion;
    }

    public void setAnotacion(String anotacion) {
        this.anotacion = anotacion;
    }

    @Override
    public String toString() {
        return anotacion;
    }
    
    
}
