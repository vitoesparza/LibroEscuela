/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import java.util.ArrayList;
import java.util.Arrays;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"nombre","apellido","rut","nota","anotacion","asistencia","apoderado"})
public class Estudiante {

    private String nombre;
    private String apellido;
    private String rut;
    private Nota[] nota;
    private ArrayList<Anotacion> anotacion;
    private Asistencia asistencia;
    private Apoderado apoderado;

    public Estudiante() {
    }
    
    /**
     * crea al estudiante que petenecerá a un curso en especifico.
     * @param nombre el ingreso del nombre del estudiante
     * @param apellido el ingreso del apellido
     * @param rut el ingreso del rut
     * @param nota el ingreso de las notas que tiene el estudiante.
     * @param apoderado del apoderado del estudiante.
     */
    public Estudiante(String nombre, String apellido, String rut, Nota[] nota,
            Apoderado apoderado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
        this.nota = nota;
        this.apoderado = apoderado;
    }
    
    /**
     * 
     * @return retorna el nombre del estudiante.
     */
    @XmlElement(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * 
     * @return retorna el apellido del estudiante.
     */
    @XmlElement(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    /**
     * 
     * @return retorna el rut del estudiante
     */
    @XmlElement(name = "rut")
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }
    
    /**
     * 
     * @return retorna el apoderado del estudiante
     */
    @XmlElement(name = "apoderado")
    public Apoderado getApoderado() {
        return apoderado;
    }

    public void setApoderado(Apoderado apoderado) {
        this.apoderado = apoderado;
    }
    
    /**
     * 
     * @return retorna las notas del estudiante.
     */
    @XmlElement(name = "nota")
    public Nota[] getNota() {
        return nota;
    }

    public void setNota(Nota[] nota) {
        this.nota = nota;
    }
    /**
     * 
     * @return retorna las anotaciones del estudiante
     */
    @XmlElement(name = "anotacion")
    public ArrayList<Anotacion> getAnotacion() {
        return anotacion;
    }

    public void setAnotacion(ArrayList<Anotacion> anotacion) {
        this.anotacion = anotacion;
    }
    /**
     * 
     * @return las asistencias
     */
    @XmlElement(name = "asistencia")
    public Asistencia getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(Asistencia asistencia) {
        this.asistencia = asistencia;
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre + " " + apellido + ", Rut " + rut + "\nNotas :" + Arrays.toString(nota);
    }

}
