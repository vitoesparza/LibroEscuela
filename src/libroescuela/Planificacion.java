/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"plan"})
public class Planificacion {
    private String plan;

    public Planificacion() {
    }
    /**
     * se crea un objeto planificación
     * @param plan al cual se le insertará una planificación
     */
    public Planificacion(String plan) {
        this.plan = plan;
    }
    
    /**
     * 
     * @return retorna la planificación correspondiente.
     */
    @XmlElement(name = "plan")
    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    @Override
    public String toString() {
        return plan;
    }
    
}
