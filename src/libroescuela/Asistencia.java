/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libroescuela;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Ignacio Esparza
 */
@XmlType(propOrder = {"asistencia"})
public class Asistencia {
    private String [][] asistencia;

    public Asistencia() {
    }
    
    /**
     * se creara el objeto asistencia.
     * @param asistencia
     */
    public Asistencia(String[][] asistencia) {
        this.asistencia = asistencia;
    }
    
    /**
     * 
     * @return retornara una matriz con las asistencias respectivas.
     */
    @XmlElement(name = "asistencia")
    public String[][] getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String[][] asistencia) {
        this.asistencia = asistencia;
    }
    
    
}
